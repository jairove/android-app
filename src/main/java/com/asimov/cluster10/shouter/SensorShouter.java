package com.asimov.cluster10.shouter;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.TextView;

import com.asimov.cluster10.R;

/**
 * Created by Rebeca on 03/12/2016.
 */

public class SensorShouter extends Activity implements SensorEventListener {
    private SensorManager mSensorManager;
    private Sensor mSensor;
    private long last_update = 0, last_movement = 0;
    private float prevX = 0, prevY = 0, prevZ = 0;
    private float curX = 0, curY = 0, curZ = 0;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        //Inicializamos el listener y el sensor
        setContentView(R.layout.activity_shouter);
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }
    @Override
    public void onSensorChanged(SensorEvent evento) {
        synchronized (this) {
            if(Sensor.TYPE_ACCELEROMETER == evento.sensor.getType()) {
                long current_time = evento.timestamp;
                curX = evento.values[0];
                curY = evento.values[1];
                curZ = evento.values[2];

                if (prevX == 0 && prevY == 0 && prevZ == 0) {
                    last_update = current_time;
                    last_movement = current_time;
                    prevX = curX;
                    prevY = curY;
                    prevZ = curZ;
                }

                long time_difference = current_time - last_update;
                if (time_difference > 0) {
                    float movement = Math.abs((curX + curY + curZ) - (prevX - prevY - prevZ)) / time_difference;
                    float min_movement = 1E-3f;
                    if (movement > min_movement)
                        last_movement = current_time;
                    prevX = curX;
                    prevY = curY;
                    prevZ = curZ;
                    last_update = current_time;
                }

                ((TextView) findViewById(R.id.coordenadaX)).setText("X:" + evento.values[0]);
                ((TextView) findViewById(R.id.coordenadaY)).setText("Y:" + evento.values[1]);
                ((TextView) findViewById(R.id.coordenadaZ)).setText("Z:" + evento.values[2]);
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) { }

    @Override
    protected void onStop() {
        SensorManager sm = (SensorManager) getSystemService(SENSOR_SERVICE);
        sm.unregisterListener(this);
        super.onStop();
    }
}
