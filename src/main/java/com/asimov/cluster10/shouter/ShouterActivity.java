package com.asimov.cluster10.shouter;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.asimov.cluster10.R;

public class ShouterActivity extends AppCompatActivity implements SensorEventListener{
    private VistaShouter camPreview;
    private RelativeLayout mainLayout;
    private SensorManager mSensorManager;
    private Sensor mSensor;
    private long last_update = 0;
    private float prevX = 0, prevY = 0, prevZ = 0;
    private float curX = 0, curY = 0, curZ = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Set this APK no title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_shouter);

        mainLayout = (RelativeLayout) findViewById(R.id.layout);

        //Inicializamos el listener y el sensor
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);

        camPreview = new VistaShouter(ShouterActivity.this);
        mainLayout.addView(camPreview);

        Button botonDisparo = (Button)findViewById(R.id.botonDisparo);
        botonDisparo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
            }
        });
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy){

    }

    @Override
    public void onSensorChanged(SensorEvent evento)
    {
            long current_time = evento.timestamp;
            curX = evento.values[0];
            curY = evento.values[1];
            curZ = evento.values[2];

            if (prevX == 0 && prevY == 0 && prevZ == 0) {
                last_update = current_time;
                prevX = curX;
                prevY = curY;
                prevZ = curZ;
            }

            long time_difference = current_time - last_update;
                if (time_difference > 0) {
                    float movement = Math.abs((curX + curY + curZ) - (prevX - prevY - prevZ)) / time_difference;
                    float min_movement = 1E-2f;
                    if (movement > min_movement){
                        ((TextView)findViewById(R.id.coordenadaX)).setText("X:"+evento.values[0]);
                        ((TextView)findViewById(R.id.coordenadaY)).setText("Y:"+evento.values[1]);
                        ((TextView)findViewById(R.id.coordenadaZ)).setText("Z:"+evento.values[2]);
                    }
                    prevX = curX;
                    prevY = curY;
                    prevZ = curZ;
                    last_update = current_time;
                }

    }
}