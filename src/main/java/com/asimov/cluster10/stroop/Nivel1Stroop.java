package com.asimov.cluster10.stroop;

import android.app.AlarmManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.asimov.cluster10.dominio.Opciones;
import com.asimov.cluster10.main.FinJuego;
import com.asimov.cluster10.R;

import java.util.ArrayList;
import java.util.Calendar;

public class Nivel1Stroop extends AppCompatActivity {

    private CountDownTimer timer;
    private MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nivel1_stroop);
        if (Opciones.getOpciones().isMusica()) {
            mediaPlayer = MediaPlayer.create(this,R.raw.omfgdogs);
            mediaPlayer.setLooping(true);
            mediaPlayer.setVolume(50,50);
            mediaPlayer.start();
        }

        ArrayList<TextView> palabras = new ArrayList<>();

        TextView puntuacionText = (TextView) findViewById(R.id.puntuacion);

        palabras.add((TextView) findViewById(R.id.palabra1));
        palabras.add((TextView) findViewById(R.id.palabra2));
        palabras.add((TextView) findViewById(R.id.palabra3));
        palabras.add((TextView) findViewById(R.id.palabra4));
        palabras.add((TextView) findViewById(R.id.palabra5));
        palabras.add((TextView) findViewById(R.id.palabra6));
        palabras.add((TextView) findViewById(R.id.palabra7));
        palabras.add((TextView) findViewById(R.id.palabra8));

        String [] nombresColores = {"Rojo", "Azul", "Verde", "Amarillo", "Negro", "Naranja", "Morado", "Rosa"};
        int [] colores = {Color.RED, Color.BLUE, Color.parseColor("#3CFF00"), Color.parseColor("#FFDA00"),
                Color.BLACK, Color.parseColor("#FF8400"), Color.parseColor("#863AA2"), Color.parseColor("#EF00FF")};
        ArrayList<Integer> coloresRestantes = new ArrayList<>();
        ArrayList<String> nombresRestantes = new ArrayList<>();

        createGame(palabras, colores, coloresRestantes, nombresColores, nombresRestantes, puntuacionText);

        Calendar calendar = Calendar.getInstance();
        Long preTime = calendar.getTimeInMillis();

        // Se dan 20s
        calendar.add(Calendar.SECOND, 20);
        Long postTime = calendar.getTimeInMillis();
        Long delay = postTime - preTime;

        AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        manager.set(AlarmManager.RTC_WAKEUP, postTime, null);

        timer = new CountDownTimer(delay, 1) {
            @Override
            public void onTick(long millisUntilFinished) {
                final int seconds = (int) (millisUntilFinished / 1000) % 60;
                final int decSegundos = (int) (millisUntilFinished / 100) % 10;
                TextView textoTiempo = (TextView)findViewById(R.id.textoTiempo);
                textoTiempo.setText(seconds+"."+decSegundos);
            }

            @Override
            public void onFinish() {
                apagarMusica();
                Intent i = new Intent(getBaseContext(), FinJuego.class);
                i.putExtra("puntuacion",((TextView) findViewById(R.id.puntuacion)).getText());
                i.putExtra("nombreJuego", "ping-pong");
                finish();
                startActivity(i);
            }
        };
        timer.start();
    }

    private void createGame(final ArrayList<TextView> palabras, final int [] colores, final ArrayList<Integer> coloresRestantes,
                            final String[] nombresColores, final ArrayList<String> nombresRestantes, final TextView puntuacionText) {
        for (int i = 0; i < colores.length; i++) {
            coloresRestantes.add(colores[i]);
            nombresRestantes.add(nombresColores[i]);
        }

        // Se elije un color aleatorio entre 0 y 7 a buscar.
        int colorAleatorio = (int)(Math.random() * 8);
        TextView color = (TextView) findViewById(R.id.color);
        color.setTextColor(coloresRestantes.get(colorAleatorio));
        color.setText(nombresColores[colorAleatorio]);

        // Elegimos una palabra correcta de las 6 que hay y la determinamos el color sacado antes.
        int palabraElegida = (int)(Math.random() * 8);
        TextView palabraCorrecta = palabras.get(palabraElegida);
        palabraCorrecta.setTextColor(coloresRestantes.get(colorAleatorio));

        palabraCorrecta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                puntuacionText.setText(String.valueOf(Integer.parseInt((String) puntuacionText.getText())+50));
                createGame(palabras, colores, coloresRestantes, nombresColores, nombresRestantes, puntuacionText);
            }
        });

        // Borramos el color que se busca como correcto de los colores.
        coloresRestantes.remove(colorAleatorio);
        int maxValorRandom = 7;

        //Establecemos el color a todas las demas palabras restantes.
        for (TextView palabra: palabras) {
            if (!palabra.equals(palabraCorrecta)) {
                int c;
                c = (int)(Math.random() * maxValorRandom);
                palabra.setTextColor(coloresRestantes.get(c));
                palabra.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish(v);
                    }
                });
                coloresRestantes.remove(coloresRestantes.get(c));
                maxValorRandom--;
            }
        }

        int c = palabras.get(palabras.size()-1).getCurrentTextColor();
        String colorPeligroso = "";
        for (int i = 0; i < colores.length; i++) {
            if (colores[i] == c) {
                colorPeligroso = nombresColores[i];
            }
        }

        for (int colore : colores) {
            coloresRestantes.add(colore);
        }

        // Establecemos un texto diferente al color de la palabra.
        maxValorRandom = 8;
        for (TextView palabra: palabras) {
            if (palabras.indexOf(palabra) == palabras.size()-2 && nombresRestantes.contains(colorPeligroso)) {
                // Especificamos el color peligroso para no conseguir bloqueos despues.
                palabra.setText(colorPeligroso);
                nombresRestantes.remove(colorPeligroso);
                coloresRestantes.remove(new Integer(c));
            } else {
                int texto;
                do {
                    texto = (int) (Math.random() * maxValorRandom);
                } while (palabra.getCurrentTextColor() == coloresRestantes.get(texto));
                palabra.setText(nombresRestantes.get(texto));
                nombresRestantes.remove(texto);
                coloresRestantes.remove(texto);
            }
            maxValorRandom--;
        }
    }

    @Override
    public void onBackPressed() {
        timer.cancel();
        apagarMusica();
        super.onBackPressed();
    }

    public void finish(View v) {
        timer.cancel();
        apagarMusica();
        Intent i = new Intent(this, FinJuego.class );
        i.putExtra("puntuacion",((TextView) findViewById(R.id.puntuacion)).getText());
        finish();
        startActivity(i);
    }

    public void apagarMusica() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
        }
    }
}
