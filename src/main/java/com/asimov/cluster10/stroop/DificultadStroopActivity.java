package com.asimov.cluster10.stroop;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.asimov.cluster10.R;

import java.util.ArrayList;
import java.util.List;

public class DificultadStroopActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private String nivel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dificultad_stroop);

        // Spinner element
        Spinner spinner = (Spinner) findViewById(R.id.elegirNivel);

        // Spinner click listener
        spinner.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<>();
        categories.add("Nivel 1");
        categories.add("Nivel 2");
        categories.add("Nivel 3");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a spinner item
        this.nivel = parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        this.nivel = parent.getItemAtPosition(0).toString();
    }

    public void elegir(View v) {
        Intent i;
        switch(nivel) {
            case "Nivel 1":
                i = new Intent(this, Nivel1Stroop.class);
                break;
            case "Nivel 2":
                i = new Intent(this, Nivel2Stroop.class);
                break;
            case "Nivel 3":
                i = new Intent(this, Nivel3Stroop.class);
                break;
            default:
                i = new Intent(this, Nivel1Stroop.class);
                break;
        }
        startActivity(i);
    }
}
