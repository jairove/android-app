package com.asimov.cluster10.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.asimov.cluster10.R;

/**
 * Created by Rebeca on 09/12/2016.
 */

public class AyudaShouterActivity extends AppCompatActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ayuda_shouter);
    }

    public void volver(View v) {
        Intent i = new Intent(this, OpcionesActivity.class );
        startActivity(i);
    }
}
