package com.asimov.cluster10.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.asimov.cluster10.pingpong.PingPongActivity;
import com.asimov.cluster10.puzzle.PuzzleActivity;
import com.asimov.cluster10.shouter.ShouterActivity;
import com.asimov.cluster10.stroop.DificultadStroopActivity;
import com.asimov.cluster10.R;

public class JuegosActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_juegos);
    }

    public void stroop(View v) {
        Intent i = new Intent(this, DificultadStroopActivity.class );
        startActivity(i);
    }

    public void pingpong(View v) {
        Intent i = new Intent(this, PingPongActivity.class );
        startActivity(i);
    }

    public void puzzle(View v) {
        Intent i = new Intent(this, PuzzleActivity.class );
        startActivity(i);
    }

    public void shouter(View v) {
        Intent i = new Intent(this, ShouterActivity.class );
        startActivity(i);
    }
}
