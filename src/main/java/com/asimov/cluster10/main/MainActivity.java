package com.asimov.cluster10.main;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.asimov.cluster10.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void jugar(View v) {
        Intent i = new Intent(this, JuegosActivity.class );
        startActivity(i);
    }

    public void opciones(View v) {
        Intent i = new Intent(this, OpcionesActivity.class );
        startActivity(i);
    }

    public void salir(View v) {
        finish();
        System.exit(0);
    }
}
