package com.asimov.cluster10.main;

import android.support.v4.app.ShareCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.asimov.cluster10.R;

public class FinJuego extends AppCompatActivity {

    CharSequence nombreJuego;
    CharSequence puntuacion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fin_juego);
        this.nombreJuego = getIntent().getCharSequenceExtra("nombreJuego");
        this.puntuacion = getIntent().getCharSequenceExtra("puntuacion");
        //TODO: Mover la cadena a strings.xml
        ((TextView) findViewById(R.id.puntuacionFinal)).setText("Puntuación final:\b"+puntuacion);
    }

    public void aceptar(View v) {
        onBackPressed();
    }

    public void compartir(View v) {
        //TODO: Mover la cadena a strings.xml
        String mensaje = "He conseguido una puntuación de\b"+puntuacion+"\ben el juego\b"+nombreJuego;
        // Creacion del intent para compartir
        ShareCompat.IntentBuilder
                .from(this) // desde esta activity
                .setText(mensaje)
                .setType("text/plain") // tipo MIME
                .setChooserTitle("Share")
                .startChooser();
    }
}
