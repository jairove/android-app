package com.asimov.cluster10.puzzle;
import android.content.Context;
import android.view.MotionEvent;

import org.cocos2d.actions.base.CCAction;
import org.cocos2d.actions.instant.CCCallFuncN;
import org.cocos2d.actions.interval.CCMoveTo;
import org.cocos2d.actions.interval.CCSequence;
import org.cocos2d.layers.CCLayer;
import org.cocos2d.layers.CCScene;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCNode;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.opengl.CCBitmapFontAtlas;
import org.cocos2d.types.CGPoint;
import org.cocos2d.types.CGRect;
import org.cocos2d.types.CGSize;
import org.cocos2d.types.ccColor3B;

import java.util.ArrayList;


/**
 * @author cluster10
 * Clase que implementa la plantilla y funcionalidad del puzzle
 */

public class PuzzleLayer extends CCLayer {
    private static float ALTURA_PIEZA;  // Altura de las piezas
    private static final int NUM_ROWS = 4;    // Numero de filas
    private static final int NUM_COL = 4;  // Numero de columnas
    private static final int NODE_TAG = 23;  // Tag para identificar al node que contiene las fichas
    private static CGPoint posVacia ;   // Representa la posición en la que no hay pieza
    private static boolean finJuego; // Flag que determina el fin de juego
    private int toppoint = 0; //Coordenada en la que empezar a colocar las piezas
    private int topleft = 0;
    private float factorEscala = 0.0f; // Factor de escala para el dibujado
    private static CGSize screenSize;
    private PuzzleActivity context;

    private int movimientos;
    private int movrestantes;
    private CCBitmapFontAtlas movimientosLabel, movValorLabel, movrestantesLabel, resValorLabel;

    private PuzzleLayer(Context context) {
        finJuego = false;

        this.context = (PuzzleActivity) context;
        screenSize = CCDirector.sharedDirector().winSize();
        factorEscala  = CCDirector.sharedDirector().winSize().height / 500 ;

/*        CCSprite fondo = CCSprite.sprite("background.jpg");
        fondo.setScale(screenSize.width / fondo.getContentSize().width);
        fondo.setAnchorPoint(CGPoint.ccp(0f,1f)) ;
        fondo.setPosition(CGPoint.ccp(0, screenSize.height));
        addChild(fondo,-5);*/
        float posX = screenSize.width - 25*factorEscala;

        movimientosLabel = CCBitmapFontAtlas.bitmapFontAtlas("Movimientos:","bionic.fnt");
        movimientosLabel.setColor(ccColor3B.ccWHITE);
        movimientosLabel.setScale(1.3f*factorEscala);
        movimientosLabel.setAnchorPoint(CGPoint.ccp(1f,1f));
        movimientosLabel.setPosition(CGPoint.ccp(posX, screenSize.height -
                10*factorEscala ));
        addChild(movimientosLabel,-2);
        float alturaMovimientos =  screenSize.height-movimientosLabel.getContentSize().height*1.3f;

        movValorLabel = CCBitmapFontAtlas.bitmapFontAtlas("0","bionic.fnt");
        movValorLabel.setColor(ccColor3B.ccWHITE);
        movValorLabel.setScale(1.3f*factorEscala);
        movValorLabel.setAnchorPoint(CGPoint.ccp(1f,1f));
        movValorLabel.setPosition(CGPoint.ccp(posX, alturaMovimientos-40*factorEscala));
        addChild(movValorLabel,-2);

        movrestantes = 200;

        movrestantesLabel = CCBitmapFontAtlas.bitmapFontAtlas("Restantes:","bionic.fnt");
        movrestantesLabel.setScale(1.3f*factorEscala);
        movrestantesLabel.setColor(ccColor3B.ccGREEN);
        movrestantesLabel.setAnchorPoint(CGPoint.ccp(1f,1f));
        movrestantesLabel.setPosition(CGPoint.ccp(posX, alturaMovimientos-85*factorEscala));
        addChild(movrestantesLabel,-2);

        resValorLabel = CCBitmapFontAtlas.bitmapFontAtlas("100","bionic.fnt");
        resValorLabel.setScale(1.3f*factorEscala);
        resValorLabel.setColor(ccColor3B.ccGREEN);
        resValorLabel.setAnchorPoint(CGPoint.ccp(1f,1f));
        resValorLabel.setPosition(CGPoint.ccp(posX, alturaMovimientos-135*factorEscala));
        addChild(resValorLabel,-2);
        
        this.generarPiezas();
        this.setIsTouchEnabled(true);
    }


    public static CCScene scene(Context context)
    {
        CCScene scene = CCScene.node();
        PuzzleLayer layer = new PuzzleLayer(context);
        scene.addChild(layer);
        return scene;
    }

    //Se rescribe el método que gestiona los toques en pantalla
    @Override
    public boolean ccTouchesBegan(MotionEvent event)
    {
        CGPoint location = CCDirector.sharedDirector().convertToGL(CGPoint.ccp(event.getX(), event.getY()));
        CGRect rectangulo;

        CCNode piezaNodo = getChild(NODE_TAG);

        //Se recorren las piezas, asignando una zona rectangular a cada una y comprobando si el toque está en ese rectángulo
        for (int i = 1 ; i < (NUM_ROWS * NUM_COL); i++){
            Nodo eachNode = (Nodo) piezaNodo.getChild(i) ;

            //Se construye un rectángulo en las inmediaciones de las coordenadas de la pieza
            rectangulo = CGRect.make(
                    eachNode.getPosition().x - (eachNode.getContentSize().width*factorEscala/2.0f),
                    eachNode.getPosition().y - (eachNode.getContentSize().height*factorEscala/2.0f),
                    eachNode.getContentSize().width*factorEscala,
                    eachNode.getContentSize().height*factorEscala);
            // Comprueba si la posición coincide con la pieza actual
            if(rectangulo.contains(location.x, location.y)){
                deslizarPiezaCallback(eachNode); // Se desliza la pieza
            }
        }
        return true ;
    }

    /**
     * Función callback que se invoca al pulsar un nodo
     * @param nodo
     */
    private void deslizarPiezaCallback(Nodo nodo) {

        CGPoint posicion = nodo.getPosition();

        // Comprueba que la posición vacia es contigua en alguna dirección de las posibles
        if((posicion.x - ALTURA_PIEZA)== posVacia.x && posicion.y == posVacia.y){
            deslizarPieza(nodo,true);
        }else if((posicion.x + ALTURA_PIEZA) == posVacia.x && posicion.y == posVacia.y){
            deslizarPieza(nodo,true);
        }else if((posicion.x)== posVacia.x && posicion.y == (posVacia.y  + ALTURA_PIEZA )){
            deslizarPieza(nodo,true);
        }else if((posicion.x )== posVacia.x && posicion.y == (posVacia.y  - ALTURA_PIEZA)){
            deslizarPieza(nodo,true);
        }else{
            deslizarPieza(nodo,false);
        }

    }

    /**
     * Desplaza la posición de una pieza
     * @param nodo nodo de la pieza
     * @param movible determina si la pieza es movible
     */
    private void deslizarPieza(Nodo nodo, boolean movible){
        if(movible && !finJuego){

            //Actuliza labels
            movimientos++;
            movrestantes--;
            // TODO sacar este string a strings.xml
            movValorLabel.setString(Integer.toString(movimientos));
            resValorLabel.setString(Integer.toString(movrestantes));
            if(movrestantes<25) {
                movrestantesLabel.setColor(ccColor3B.ccRED);
                resValorLabel.setColor(ccColor3B.ccRED);
            }
            if(movrestantes==0) {
                movimientosLabel.setString("GAME OVER");
                setIsTouchEnabled(false);
                return;
            }
            CGPoint nodePosition = nodo.getPosition();
            CGPoint tempPosition = posVacia ;
            CCMoveTo movetile = CCMoveTo.action(0.4f, tempPosition);
            //finJuego = checkCorrect();
            CCSequence movetileSeq = CCSequence.actions(movetile, CCCallFuncN.action(this, "finCallback"));
            nodo.runAction(movetileSeq);
            posVacia = nodePosition ;
        }
        else if(finJuego) {
            movimientosLabel.setString("VICTORIA");
            removeAllChildren(true);
            finJuego = false;
            context.finJuego((int) Math.floor(movimientos/200));
        }
    }

    public void finCallback(Object sender){
        boolean fin = comprobarPosiciones();
        if(fin) finJuego = true;
    }

    private Integer[] generarJuego(){
        // Algoritmo para generar un conjunto de numeros solucionables para el puzzle.
        boolean solucionable = false;
        int[] numerosPuzzle = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
        ArrayList<Integer> numerosRestantes = new ArrayList<>();
        ArrayList<Integer> numerosPiezas = new ArrayList<>();
        int numeroRandom,maxRandom,inversiones;

        do {
            maxRandom = 16;
            inversiones = 0;
            numerosRestantes.clear();
            for (int aNumerosPuzzle : numerosPuzzle) {
                numerosRestantes.add(aNumerosPuzzle);
            }
            for (int i = 0; i < 16; i++) {
                numeroRandom = (int)(Math.random() * maxRandom);
                numerosPiezas.add(numerosRestantes.get(numeroRandom));
                numerosRestantes.remove(numerosRestantes.get(numeroRandom));
                maxRandom--;
            }
            for (int i = 0; i < numerosPiezas.size()-1; i++) {
                for (int j = i+1; j < numerosPiezas.size(); j++) {
                    if (numerosPiezas.get(i) > numerosPiezas.get(j)) {
                        inversiones++;
                    }
                }
            }
            if (inversiones % 2 == 0) {
                // Tiene solucion.
                solucionable = true;
            }
        } while(!solucionable);

        Integer[] numeros = new Integer[numerosPiezas.size()];  //La secuencia aleatoria a resolver
        numerosPiezas.toArray(numeros);
        return numeros;
    }

    /**
     * Comprueba si el puzzle se ha resuelto, comparando las posiciones de las piezas
     */
    public boolean comprobarPosiciones() {
        CCNode nodoPiezas = getChild(NODE_TAG);
        int index = 1;
        boolean victoria = false;
        //Index es el tag de la ficha y también su valor

        for (float j = toppoint; j > toppoint - (ALTURA_PIEZA * NUM_ROWS); j -= ALTURA_PIEZA) {
            for (float i = topleft; i < (topleft - 5) + (ALTURA_PIEZA * NUM_COL); i += ALTURA_PIEZA) {
                if (nodoPiezas.getChild(index).getPosition().x == i &&
                        nodoPiezas.getChild(index).getPosition().y == j) {
                    System.out.println(nodoPiezas.getChild(index).getPosition().x);
                    victoria = true;
                }
                else return false;
                index++;
                if (index == (NUM_ROWS * NUM_COL)) return victoria;
            }
        }
        return victoria;
    }

    /**
     * Genera las piezas del juego, así como un conjunto solucionable
     */
    private void generarPiezas(){
        // Se crea un elemento nodo que contendrá las piezas
        CCNode nodePiezas = CCNode.node();
        nodePiezas.setTag(NODE_TAG);
        addChild(nodePiezas);
        float factorEscalaPieza ;
        int indexPieza = 0 ;
        int nextval ;


        // Se calculan tamaños
        int anchura = (int) (screenSize.width - movimientosLabel.getContentSize().width*factorEscala) ;
        int altura =  (int) (screenSize.height  - 40*factorEscala) ;
        ALTURA_PIEZA = Math.min((altura/NUM_ROWS),(anchura/ NUM_COL));
        toppoint = (int) (altura-(ALTURA_PIEZA/2)+15*factorEscala);
        factorEscalaPieza = ALTURA_PIEZA/150.0f;
        topleft = (int) ((ALTURA_PIEZA/2)+15*factorEscala);

        CCSprite pieza = CCSprite.sprite("pieza.png");

        //Se obtiene una combinación de juego aleatoria
        Integer[] nuevoJuego = generarJuego();
        //Integer[] nuevoJuego = {1,2,3,4,5,6,7,8,9,10,11,0,13,14,15,12};

        for (int j = toppoint; j>toppoint-(ALTURA_PIEZA*NUM_ROWS); j-=ALTURA_PIEZA){
            for (int i = topleft; i<(topleft-5*factorEscala) + (ALTURA_PIEZA * NUM_COL); i+= ALTURA_PIEZA) {
                if (indexPieza >= (NUM_ROWS * NUM_COL)) break;

                // Se crea un nodo para cada pieza
                nextval = nuevoJuego[indexPieza];
                Nodo nodo =  new  Nodo();
                nodo.setContentSize(pieza.getContentSize());

                //Se posiciona el nodo y se le da valor
                nodo.setPosition(i, j);
                nodo.setValorPieza(nextval);

                //Se añade el número a la pieza
                CCBitmapFontAtlas numPieza = CCBitmapFontAtlas.bitmapFontAtlas ("00", "bionic.fnt");
                numPieza.setScale(1.4f);
                numPieza.setColor(ccColor3B.ccBLACK);
                nodo.setScale(factorEscalaPieza);
                nodo.addChild(pieza,1,1);
                numPieza.setString(nextval + "");
                nodo.addChild(numPieza,2 );

                if(nextval != 0) nodePiezas.addChild(nodo,1,nextval);
                else posVacia = CGPoint.ccp(i, j);
                indexPieza++;
            }
        }
    }
}
