package com.asimov.cluster10.puzzle;

import org.cocos2d.nodes.CCNode;

/**
 * @author cluster10
 * Clase que extiende a CCNode para permitir almacenar texto en un nodo
 */
public class Nodo extends CCNode{
    public int valorNodo;

    public Nodo() {
        super();
    }

    public void setValorPieza(int valor){

        this.valorNodo = valor;
    }

    public int getValorPieza(){
        return this.valorNodo ;
    }

}
