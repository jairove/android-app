package com.asimov.cluster10.puzzle;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.asimov.cluster10.main.FinJuego;

import org.cocos2d.layers.CCScene;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.opengl.CCGLSurfaceView;

/**
 * @author Cluster10
 * Activity del puzzle
 */

public class PuzzleActivity extends AppCompatActivity {
    protected CCGLSurfaceView _glSurfaceView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        _glSurfaceView = new CCGLSurfaceView(this);
        setContentView(_glSurfaceView);

        CCDirector director = CCDirector.sharedDirector();
        director.attachInView(_glSurfaceView);
        director.setDeviceOrientation(CCDirector.kCCDeviceOrientationLandscapeLeft); // Orientación horizontal
        CCDirector.sharedDirector().setAnimationInterval(1.0f / 60.0f);  // Se fijan 60fps

        CCScene scene = PuzzleLayer.scene(this); //
        CCDirector.sharedDirector().runWithScene(scene);
    }

    @Override
    public void onPause()
    {
        super.onPause();
        CCDirector.sharedDirector().pause();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        CCDirector.sharedDirector().resume();
    }

    @Override
    public void onStop()
    {
        super.onStop();
        CCDirector.sharedDirector().end();
    }

    public void finJuego(int puntuacion) {
        CCDirector.sharedDirector().purgeCachedData();
        CCDirector.sharedDirector().end();
        Intent i = new Intent(this, FinJuego.class );
        i.putExtra("puntuacion", String.valueOf(puntuacion));
        i.putExtra("nombreJuego", "Puzzle!");
        finish();
        startActivity(i);
    }
}
