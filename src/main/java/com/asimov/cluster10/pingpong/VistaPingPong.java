package com.asimov.cluster10.pingpong;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;
import com.asimov.cluster10.R;


public class VistaPingPong extends View {
    private int screenW = 0;
    private int screenH = 0;
    private int X, Y, jugadorY, velocidadJugador, movimientoJugador, cpuY, initialY, batW, ballW, batH, ballH, batInitPos, puntuacion;
    float dX, dY;
    Bitmap ball, Bat, bgr;
    PingPongActivity pingpongActivity;

    public VistaPingPong(Context context) {
        super(context);
        puntuacion = 0;

        // Carga imagenes
        ball = BitmapFactory.decodeResource(getResources(), R.drawable.ball);
        Bat = BitmapFactory.decodeResource(getResources(), R.drawable.cpubat);
        bgr = BitmapFactory.decodeResource(getResources(), R.drawable.pingpongtable);
        ballW = ball.getWidth();
        ballH = ball.getHeight();
        batW = Bat.getWidth();
        batH = Bat.getHeight();
        batInitPos = 50;
        velocidadJugador = 10;
        dY = 5; //velocidad vertical
        dX = 10; //velocidad horizontal
        initialY = 100; // posición vertical inicial
        pingpongActivity = (PingPongActivity) context;
    }

    @Override
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        screenW = w;
        screenH = h;
        bgr = Bitmap.createScaledBitmap(bgr, w, h, true); // Resize del fondo
        X = (screenW / 2) - (ballW / 2); // Para centrar la bola
        cpuY = initialY - batH / 2;
        jugadorY = initialY - batH / 2;
        Y = initialY - ballH / 2;
    }

    /**
     * Método que se encarga del dibujado del juego
     * @param canvas
     */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        //Se calcula velocidad y posición de la bola
        if (jugadorY + movimientoJugador > (screenH - batH)) {
            jugadorY = screenH - batH;
        } else {
            if (jugadorY + movimientoJugador < 0)
                jugadorY = 0;
            else
                jugadorY += movimientoJugador;
        }


        if (Y + dY > (screenH - ballH)) {
            Y = (screenH - ballH) * 2 - (Y + (int) dY);
            dY = (-1) * dY; // Hay un toque, se invierte la velocidad
        } else {
            if (Y + dY < 0) {
                Y = -(int) dY - Y;
                dY = (-1) * dY; // Hay un toque, se invierte la velocidad
            } else {
                Y += (int) dY; // Cambio de posición vertical (incremento o decremento)
            }
        }

        if (X + dX > (screenW - batInitPos - ballW)) {
            X = (screenW - batInitPos - ballW) * 2 - (X + (int) dX);
            dX = (-1) * dX; // Hay un toque, se invierte la velocidad
        } else {
            if (X + dX < batInitPos + batW - ballW / 2) {
                if ((jugadorY - Y + batH < 0) || (jugadorY - Y > 0)) {
                    pingpongActivity.finJuego(puntuacion);
                }
                puntuacion++;

                //Incremento de dificultad segun la puntuacion
                if (puntuacion % 5 == 0) {
                    dX = dX - 5;
                }

                dX = (-1) * dX; // Hay un toque, se invierte la velocidad
            } else {
                X += (int) dX; // Cambio de posición horizontal (incremento o decremento)
            }
        }

        if ((Y < (screenH - batH / 2) && (Y > batH / 2))) {
            cpuY = Y - batH / 2;
        }

        canvas.drawBitmap(bgr, 0, 0, null);

        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setTextSize(30);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);

        canvas.drawText(" Score: " + puntuacion, screenW / 2 - 400, screenH-15, paint);
        canvas.drawBitmap(ball, X, Y, null);
        canvas.drawBitmap(Bat, batInitPos, jugadorY, null);
        canvas.drawBitmap(Bat, screenW - batInitPos, cpuY, null);

        invalidate();
    }

    /**
     * Gestiona los toques en pantalla
     * @param event
     * @return
     */
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction();
        if (action == MotionEvent.ACTION_DOWN) {
            if (jugadorY - (int) event.getY() < 0)
                movimientoJugador = velocidadJugador;
            if (jugadorY - (int) event.getY() > 0)
                movimientoJugador = (-1) * velocidadJugador;
            invalidate();
            return true;
        }
        if (action == MotionEvent.ACTION_UP) {
            movimientoJugador = 0;
        }
        return false;
    }
}
