package com.asimov.cluster10.pingpong;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.asimov.cluster10.main.FinJuego;


public class PingPongActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        VistaPingPong vista = new VistaPingPong(this);
        setContentView(vista);
    }

    public void aceptar(View v) {
        onBackPressed();
    }

    public void finJuego(int puntuacion) {
        Intent i = new Intent(this, FinJuego.class );
        i.putExtra("puntuacion", String.valueOf(puntuacion));
        i.putExtra("nombreJuego", "ping-pong");
        finish();
        startActivity(i);
    }
}
