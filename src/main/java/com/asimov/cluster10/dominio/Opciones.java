package com.asimov.cluster10.dominio;

/**
 * Created by mario on 2/12/16.
 */

public class Opciones {

    private static Opciones opciones;
    private boolean musica;

    public static Opciones getOpciones() {
        if (opciones == null) {
            opciones = new Opciones();
        }
        return opciones;
    }

    private Opciones () {
        musica = true;
    }

    public boolean isMusica() {
        return musica;
    }

    public void setMusica(boolean musica) {
        this.musica = musica;
    }

}
